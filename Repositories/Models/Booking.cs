﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Repositories.Models
{
    public class Booking
    {
        public int Id { get; set; }

        public DateTime From { get; set; }

        public DateTime Till { get; set; }

        public string Guest { get; set; }

        public Property Property { get; set; }
    }
}
