﻿using log4net;
using log4net.Config;
using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Text;

namespace Logger
{
    public sealed class Log4net : ILog4net
    {
        private Log4net()
        {
            var logRepository = LogManager.GetRepository(Assembly.GetEntryAssembly());
            XmlConfigurator.Configure(logRepository, new FileInfo("log4net.config"));
        }

        private static readonly Lazy<Log4net> instance = new Lazy<Log4net>(() => new Log4net());
        private static readonly ILog _logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public static Log4net GetInstance
        {
            get
            {
                return instance.Value;
            }
        }

        public void HandleException(Exception ex)
        {
            Log("Error", GetErrorMessageFromException(ex), TraceEventType.Error);
        }

        private string GetErrorMessageFromException(Exception ex)
        {
            var errorMessage = new StringBuilder("Message: ");
            errorMessage.Append(ex.Message);
            errorMessage.Append(Environment.NewLine);
            errorMessage.Append("Exception Type: ");
            errorMessage.Append(ex.GetType().ToString());
            errorMessage.Append(Environment.NewLine);
            errorMessage.Append("Source: ");
            errorMessage.Append(ex.Source);
            errorMessage.Append(Environment.NewLine);
            errorMessage.Append("StacktTrace: ");
            errorMessage.Append(Environment.NewLine);
            errorMessage.Append(ex.StackTrace);
            errorMessage.Append(Environment.NewLine);

            if (ex.InnerException != null)
            {
                errorMessage.Append("InnerException Message: ");
                errorMessage.Append(ex.InnerException.Message);
                errorMessage.Append(Environment.NewLine);
                errorMessage.Append("InnerException Type: ");
                errorMessage.Append(ex.InnerException.GetType().ToString());
                errorMessage.Append(Environment.NewLine);
                errorMessage.Append("InnerException Source: ");
                errorMessage.Append(ex.InnerException.Source);
                errorMessage.Append(Environment.NewLine);
                errorMessage.Append("InnerException StackTrace: ");
                errorMessage.Append(Environment.NewLine);
                errorMessage.Append(ex.InnerException.StackTrace);
                errorMessage.Append(Environment.NewLine);

                if (ex.InnerException.InnerException != null)
                {
                    errorMessage.Append("InnerException InnerException Message: ");
                    errorMessage.Append(ex.InnerException.InnerException.Message);
                    errorMessage.Append(Environment.NewLine);
                    errorMessage.Append("InnerException InnerException Type: ");
                    errorMessage.Append(ex.InnerException.InnerException.GetType().ToString());
                    errorMessage.Append(Environment.NewLine);
                    errorMessage.Append("InnerException InnerException Source: ");
                    errorMessage.Append(ex.InnerException.InnerException.Source);
                    errorMessage.Append(Environment.NewLine);
                    errorMessage.Append("InnerException InnerException StackTrace: ");
                    errorMessage.Append(Environment.NewLine);
                    errorMessage.Append(ex.InnerException.InnerException.StackTrace);
                    errorMessage.Append(Environment.NewLine);
                }
            }

            return errorMessage.ToString();
        }

        private static void Log(string title, string message, TraceEventType eventType)
        {

            var logEntry = new StringBuilder("\nTitle: " + title);
            logEntry.AppendLine("\nMessage: " + message);
            logEntry.AppendLine("Severity: " + eventType);

            switch (eventType)
            {
                case TraceEventType.Information:
                    {
                        _logger.Info(logEntry);
                        break;
                    }
                case TraceEventType.Error:
                    {
                        _logger.Error(logEntry);
                        break;
                    }
                default:
                    {
                        _logger.Error(logEntry);
                        break;
                    }
            }
        }
    }
}
