﻿using System;

namespace Logger
{
    public interface ILog4net
    {
        void HandleException(Exception ex);
    }
}
