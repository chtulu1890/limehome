﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data.DTO
{
    public class BookingDto
    {
        public int Id { get; set; }

        public DateTime From { get; set; }

        public DateTime Till { get; set; }

        public string Guest { get; set; }

        public int PropertyId { get; set; }
    }
}
