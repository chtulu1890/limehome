﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data.DTO
{
    public class PropertyDto
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Address { get; set; }

        public decimal Price { get; set; }

        public double Latitude { get; set; }

        public double Longitude { get; set; }
    }
}
