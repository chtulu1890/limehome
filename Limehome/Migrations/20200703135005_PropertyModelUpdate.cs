﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Limehome.Migrations
{
    public partial class PropertyModelUpdate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Name",
                table: "Properties");

            migrationBuilder.AddColumn<string>(
                name: "Category",
                table: "Properties",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Href",
                table: "Properties",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Title",
                table: "Properties",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Vicinity",
                table: "Properties",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Category",
                table: "Properties");

            migrationBuilder.DropColumn(
                name: "Href",
                table: "Properties");

            migrationBuilder.DropColumn(
                name: "Title",
                table: "Properties");

            migrationBuilder.DropColumn(
                name: "Vicinity",
                table: "Properties");

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "Properties",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
