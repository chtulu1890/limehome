﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Services.Business;

namespace Limehome.Controllers
{
    public class ApiController<TService, TDto> : Controller
    {
        protected ServiceCollection _services { get; }
        private IService<TDto> _apiService { get; }
        public ApiController(ServiceCollection services, IService<TDto> apiService)
        {
            _services = services;
            _apiService = apiService;
        }
       
    }
}