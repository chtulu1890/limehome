﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Data.DTO;
using Microsoft.AspNetCore.Mvc;
using Services.Business;

namespace Limehome.Controllers
{
    [Route("/api/properties")]
    [Produces("application/json")]
    public class PropertyController : ApiController<PropertyService, PropertyDto>
    {
        public PropertyController(ServiceCollection services, PropertyService properties) : base(services, properties)
        {
            
        }
        
        [HttpGet("{latitude}/{longitude}")]
        public async Task<IActionResult> Get([FromRoute] double latitude, [FromRoute]double longitude)
        {
            var result = await _services.PropertyService.GetAllPropertiesByLatLongAsync(latitude, longitude);
            return Ok(result);
        }

        [HttpPost]
        [Route("{propertyId}/bookings")]
        public async Task<IActionResult> Booking([FromRoute] int propertyId)
        {
            if (propertyId == 0)
                return BadRequest("Property id can't be zero.");
            var result = await _services.BookingService.GetBookingsForProperty(propertyId);
            return Ok(result);
        }
    }
}