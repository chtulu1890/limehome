﻿using System;
using System.Threading.Tasks;
using Data.DTO;
using Microsoft.AspNetCore.Mvc;
using Services.Business;

namespace Limehome.Controllers
{
    [Route("/api/bookings")]
    [Produces("application/json")]
    public class BookingController : ApiController<PropertyService, PropertyDto>
    {
        public BookingController(ServiceCollection services, BookingService bookings) : base(services, bookings)
        {
            
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] BookingDto booking)
        {
            if (booking.From == DateTime.MinValue)
                return BadRequest("From Date is required.");
            if (booking.Till == DateTime.MinValue)
                return BadRequest("From Till is required.");
            if (string.IsNullOrEmpty(booking.Guest))
                return BadRequest("Guest is required.");
            if (booking.PropertyId == 0)
                return BadRequest("Property id can't be 0.");
            var result = await _services.BookingService.CreateBooking(booking);
            return Ok(result);
        }       
    }
}