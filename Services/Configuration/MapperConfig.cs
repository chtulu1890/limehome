﻿using Data.DTO;
using Mapster;
using Repositories.Models;

namespace Services.Configuration
{
    public class MapperConfig
    {
        public static void RegisterPropertyMapping()
        {
            TypeAdapterConfig<Property, PropertyDto>.NewConfig();
            TypeAdapterConfig<PropertyDto, Property>.NewConfig();

            TypeAdapterConfig<Booking, PropertyDto>.NewConfig();
            TypeAdapterConfig<PropertyDto, Property>.NewConfig();
        }

        public static void RegisterBookingsMapping()
        {
            TypeAdapterConfig<Booking, BookingDto>.NewConfig();
            TypeAdapterConfig<BookingDto, Booking>.NewConfig();
        }
    }
}
