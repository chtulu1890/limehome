﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Services.Business
{
    public abstract class DbService<TModel, TDto, TContext> : IService<TDto>
        where TModel : class
        where TContext : DbContext
    {
        protected TContext Context { get; }
        protected DbSet<TModel> ModelRepository { get; }
        protected DbService(TContext context)
        {
            Context = context;
            ModelRepository = Context.Set<TModel>();
        }

        //public async Task<IEnumerable<TDto>> GetAllAsync()
        //{
        //    return null;
        //}
    }
}
