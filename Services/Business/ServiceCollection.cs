﻿using Repositories.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Services.Business
{
    public class ServiceCollection
    {
        public PropertyService PropertyService { get; }

        public BookingService BookingService { get; }

        public ServiceCollection(PropertyService propertyService, BookingService bookingService)
        {
            PropertyService = propertyService;
            BookingService = bookingService;
        }
    }
}
