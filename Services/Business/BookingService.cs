﻿using Data.DTO;
using GeoCoordinatePortable;
using Mapster;
using Microsoft.EntityFrameworkCore;
using Repositories;
using Repositories.Models;
using Services.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Business
{
    public class BookingService : DbService<Property, PropertyDto, ApplicationContext>
    {
        static BookingService() => MapperConfig.RegisterBookingsMapping();

        public BookingService(ApplicationContext context) : base(context)
        {

        }
      
        public async Task<BookingDto> CreateBooking(BookingDto booking)
        {
            var property = await Context.Properties.FirstOrDefaultAsync(p => p.Id == booking.PropertyId);
            var bookingDao = TypeAdapter.Adapt<Booking>(booking);
            bookingDao.Property = property;
            await Context.Bookings.AddAsync(bookingDao);
            await Context.SaveChangesAsync();
            
            return booking;
        }


        public async Task<List<BookingDto>> GetBookingsForProperty(int propertyId)
        {
            var propertyBookings = await Context.Bookings.Include(b => b.Property).Where(b => b.Property.Id == propertyId).ToListAsync();
            return propertyBookings.Select( TypeAdapter.Adapt<BookingDto>).ToList();
        }
    }
}
