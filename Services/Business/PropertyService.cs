﻿using Data.DTO;
using GeoCoordinatePortable;
using Mapster;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Repositories;
using Repositories.Models;
using Services.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Services.Business
{
    public class PropertyService : DbService<Property, PropertyDto, ApplicationContext>
    {
        private readonly IConfiguration config;
        static PropertyService() => MapperConfig.RegisterPropertyMapping();
        public PropertyService(ApplicationContext context, IConfiguration config) : base(context)
        {
            this.config = config;
        }

        public async Task<List<PropertyDto>> GetAllPropertiesByLatLongAsync(double latitude, double longitude)
        {
            var propertiesDto = new List<PropertyDto>();
            var properties  = await Context.Properties.ToListAsync();
            var coord = new GeoCoordinate(latitude, longitude);
            var meters = config.GetValue<double>("Constants:DistanceInMeters");

            foreach(var property in properties)
            {
                var propertyGeo = new GeoCoordinate(property.Latitude, property.Longitude);
                var distance = propertyGeo.GetDistanceTo(coord);
                if (distance < meters)
                    propertiesDto.Add(TypeAdapter.Adapt<PropertyDto>(property));
            }
            return propertiesDto;
        }
    }
}
